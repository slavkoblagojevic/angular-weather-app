import { Component, OnInit } from '@angular/core';
import { WeatherService } from './services/weather.service';
import { IWeatherData } from './models/weather.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  weatherData?: IWeatherData;
  location:string = 'belgrade';

  constructor(private weatherService: WeatherService) {}

  ngOnInit(): void {
    this.getData(this.location);
    this.location = '';
  }

  getData(location:string) {
    this.weatherService.getWeatherData(location).subscribe((data) => {
      console.log(data);
      this.weatherData = data;
    });
  }

  onSubmit(){
    console.log(this.location)
    this.getData(this.location);
    this.location = '';
  }
}
