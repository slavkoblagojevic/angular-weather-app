import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fahrenheitToCelsius'
})
export class FahrenheitToCelsiusPipe implements PipeTransform {

  transform(fahrenheit: number): number {
    
    const celsius = (fahrenheit-32)*5/9;
    
    return celsius;
  }

}
