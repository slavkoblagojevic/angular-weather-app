import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IWeatherData } from '../models/weather.model';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  weatherApiBaseUrl:string =  'https://weatherapi-com.p.rapidapi.com/current.json';
  XRapidAPIHostHeaderName:string = 'X-RapidAPI-Host';
  XRapidAPIHostHeaderValue:string = 'weatherapi-com.p.rapidapi.com';
  XRapidAPIKeyHeaderName:string = 'X-RapidAPI-Key';
  XRapidAPIKeyHeaderValue:string = '9199154f58msh10b74891b6eb0d8p1df40fjsne20357e78c8f';
  paramName: string = 'q';
  paramValue: string = '53.1,-0.13';
  constructor(private http: HttpClient) { }

  getWeatherData(location:string): Observable<IWeatherData>{
      return this.http.get<IWeatherData>(this.weatherApiBaseUrl, {
      headers: new HttpHeaders()
      .set(this.XRapidAPIKeyHeaderName, this.XRapidAPIKeyHeaderValue)
      .set(this.XRapidAPIHostHeaderName, this.XRapidAPIHostHeaderValue),
      params: new HttpParams()
      .set(this.paramName, location)
    });
  }
}
